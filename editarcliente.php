<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Editar cliente | EasySpa</title>   
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="css/formulariopass.css">
</head>
<body>
    <?php include "./include/header.php" ?>     
    <?php
    $nombrescliente=$_POST['nombrescliente'];
    $apellidoscliente=$_POST['apellidoscliente'];
    $rutcliente=$_POST['rutcliente'];
    $emailcliente=$_POST['emailcliente'];
    $telcliente=$_POST['telcliente'];
    $tipocliente=$_POST['tipocliente'];
    $comentariocliente=$_POST['comentariocliente'];  
    $idcliente=$_POST['idcliente'];
   
    echo("
    <form action=\"./include/editarcliente2.php\" method=\"post\">
    	<input type=\"text\" name=\"c_nombres\" placeholder=\"Nombres\" value=\"".$nombrescliente."\" required=\"required\" >
        <input type=\"text\" name=\"c_apellidos\" placeholder=\"Apellidos\" value=\"".$apellidoscliente."\" required=\"required\" >
        <input type=\"text\" name=\"c_rut\" placeholder=\"Rut\" value=".$rutcliente." required=\"required\" readonly=\"readonly\">
        <input type=\"email\" name=\"c_email\" placeholder=\"Email\" value=".$emailcliente." required=\"required\" >
        <input type=\"number\" name=\"c_telefono\" placeholder=\"Telefono\" value=".$telcliente." >
        <input type=\"text\" name=\"c_tipo\" placeholder=\"Tipo\" value=".$tipocliente." required=\"required\" >
        <input type=\"text\" name=\"c_comentario\" placeholder=\"Comentario (opcional)\" value=\"".$comentariocliente."\">
        <input type=\"hidden\" name=\"idcliente\" value=".$idcliente." required=\"required\" >
        <button type=\"submit\" name=\"editar\" class=\"btn\" >Guardar cambios</button>
    </form>");
    ?>
    <br><br><br><br><br><br><br><br>
    <?php include "./include/footer.php" ?>



</body>

</html>