<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Editar personal | EasySpa</title>
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="css/formulariopass.css">
    <link rel="stylesheet" href="css/listado.css">
    <link rel="stylesheet" href="css/formulario.css">
    <link rel="stylesheet" href="css/alpha.css">

</head>

<body>
    <?php include "./include/header.php" ?>
    <?php
    $nombrepersonal=$_POST['nombrepersonal'];
    $apellidopersonal=$_POST['apellidopersonal'];
    $rutpersonal=$_POST['rutpersonal'];
    $cargopersonal=$_POST['cargopersonal'];
    $emailpersonal=$_POST['emailpersonal'];
    $telefonopersonal=$_POST['telefonopersonal'];
    $idpersonal=$_POST['idpersonal'];

    echo ("
    <div class=\"edit_p\">
    <br>
    <h2>Usted va a editar el siguiente personal:</h2>
    <br>
    <form action=\"./include/editarpersonal2.php\" method=\"post\">
        <p>Nombre:</p>
        <input type=\"text\" name=\"nombrepersonal\" placeholder=\"Nombres\" value=\"".$nombrepersonal."\" required=\"required\" >
        <p>Apellido:</p>
        <input type=\"text\" name=\"apellidopersonal\" placeholder=\"Apellidos\" value=\"".$apellidopersonal."\" required=\"required\" >
        <p>Rut:</p>
        <input type=\"text\" name=\"rutpersonal\" placeholder=\"Rut\" value=\"".$rutpersonal."\" required=\"required\" >
        <p>Cargo:</p>
        <input type=\"text\" name=\"cargopersonal\" placeholder=\"Cargo\" value=\"".$cargopersonal."\" required=\"required\" >
        <p>Email:</p>
        <input type=\"text\" name=\"emailpersonal\" placeholder=\"Email\" value=\"".$emailpersonal."\" required=\"required\" >
        <p>Telefono:</p>
        <input type=\"number\" name=\"telefonopersonal\" palceholder=\"Telefono\" value=\"".$telefonopersonal."\" required=\"required\" >
        <input type=\"hidden\" name=\"idpersonal\" placeholder=\"ID\" value=\"".$idpersonal."\" required=\"required\" >
        <button type=\"submit\" name=\"editar\" class=\"btn\" >Guardar cambios</button>
    </form>
    <br>
    <br>
    <form class=\"btn_cancel\" action=\"./personal.php\"\" method=\"post\">
        <button type=\"submit\" name=\"cancelar\" class=\"btn\" >Cancelar</button>
    </form>
    </div>");

    ?>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <?php include "./include/footer.php" ?>



</body>

</html>