<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cambiar contraseña | EasySpa</title>   
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="css/formulariopass.css">  
</head>
<body>
    <?php include "./include/header.php" ?>     
    <?php 
    if(!isset($_SESSION["idpersonal"])){
        header("location: ./index.php");
        exit();
    }
    if(isset($_GET["status"])){
        if($_GET["status"]== "clavecambiada"){               
         echo"<p>Contraseña cambiada exitosamente</p>";            
        }
        if($_GET["status"]== "passnocoincide"){               
            echo"<p>las nuevas contraseñas no coinciden, intente nuevamente</p>";            
        }  
        if($_GET["status"]== "passincorrecta"){               
            echo"<p>contraseña actual incorrecta</p>";            
        }
    }    
        echo"<div class=\"login\">";
        echo"<form action=\"./include/cambiarpass2.php\" method=\"post\">";              
        echo"<input type=\"password\" name=\"oldpass\" placeholder=\"Escriba su actual Contraseña\" required=\"required\" />";       
        echo"<input type=\"password\" name=\"newpass\" placeholder=\"Escriba su nueva Contraseña\" required=\"required\" />";  
        echo"<input type=\"password\" name=\"newpassrepeat\" placeholder=\"Repita su nueva Contraseña\" required=\"required\" />";   
        echo"<button type=\"submit\" name=\"changepass\" class=\"btn\" >Cambiar Contraseña</button>";
        echo"</form>";
        echo"</div>";     
    ?>   
    
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <?php include "./include/footer.php" ?>



</body>

</html>