<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Registrar Reserva | EasySpa</title>
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="css/formulariopass.css">
    <link rel="stylesheet" href="css/formulario.css">

</head>
<body>
<?php include "./include/header.php" ?>
	
    <?php

    ?>
    <section id="container">
        <div class="form_register">
            <h1>Registrar Reserva</h1>

            <form action="./include/registroreserva.php" method="post">
            <input type="number" name="r_idreserva" placeholder="id_reserva" required="required" />
            <input type="number" name="r_idcliente" placeholder="id_cliente" required="required" />
            <input type="text" name="r_nombreservicio" placeholder="nombreservicio" required="required" />
            <input type="date" name="r_fechareserva" placeholder="fecha_de_reserva" required="required" />
            <input type="number" name="r_horareserva" placeholder="hora_reserva" required="required" />
            <button type="submit" name="reservar" class="btn" >Reservar</button>
    </form>

        </div>
    </section>
    

    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <?php include "./include/footer.php" ?>

</body>

</html>