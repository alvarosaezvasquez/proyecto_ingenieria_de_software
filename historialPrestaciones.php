<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Prestaciones | EasySpa</title>
    <link rel="stylesheet" href="css/alpha.css">

</head>

<body>
    <?php include "./include/header.php" ?>
    <?php include "./classes/model/Prestaciones.class.php" ?>

    <section class= "contenedor">
    <h1>Historial Prestaciones</h1>
    <div>
        <form action="./registrarPrestacion.php" class="form_register" >
            <input type="submit" value="Crear Prestacion" class="btn_registerP">
        </form>
        <form action="./buscadorprestaciones.php" class="form_search" method="POST" >
            <input type="text" name="busqueda" id="busqueda" placeholder="Buscar prestaciones..." class ="bcliente">
            <input type="submit" value="Buscar" class="btn_search">
        </form>
    </div>
    </section>
    <?php
    if(isset($_GET["status"])){
        if($_GET["status"]== "pretacionesRegistrado"){
            echo("<p class=\"alert\">Personal registrado</p>");
        }
        if($_GET["status"]== "prestacionModificado"){
            echo("<p class=\"alert\">Prestacion modificado correctamente</p>");
        }
        if($_GET["status"] == "eliminado"){
            echo("<p class=\"alert\">Prestacion eliminado correctamente</p>");
        }
    }
    ?>

    <?php
    $personal = new PrestacionesView();
    $personal->VerPrestaciones();

    ?>

    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <?php include "./include/footer.php" ?>

</body>

</html>