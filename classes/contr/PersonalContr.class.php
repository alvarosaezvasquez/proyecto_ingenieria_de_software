<?php
    class PersonalContr extends Personal {

        public function InsertarPersonal($nombre,$apellido,$rut,$cargo,$email,$telefono,$pass){//redundante pero nesesario para cumplir MVC,solo Personal toca la base de datos
            $this->createPersonal($nombre,$apellido,$rut,$cargo,$email,$telefono,$pass);
        }
        public function ExisteUsuario($email){
            return $this->userExist(strtolower($email));
        }
        public function CambiarClaveUsuario($email,$pass){
            return $this->userUpdatePass(strtolower($email),$pass);
        }
        public function login($email,$clave){
            $usuarioex = $this->userExist(strtolower($email));
            if($usuarioex === false){
                header("location: ./index.php?status=usuarioincorrecto");
                exit();
            }
            $hashpass=$usuarioex["clavepersonal"];
            $validar = password_verify($clave,$hashpass);
            if($validar===false){
                header("location: ./index.php?status=passincorrecta");
                exit();
            }
            else if ($validar === true){
                session_start();
                $_SESSION["idpersonal"]=$usuarioex["idpersonal"];
                $_SESSION["nombrepersonal"]=$usuarioex["nombrepersonal"];
                $_SESSION["cargopersonal"]=$usuarioex["cargopersonal"];
                $_SESSION["emailpersonal"]=$usuarioex["emailpersonal"];
                header("location: ./index.php");
                exit();
            }
        }
        public function eliminarToken($email){
            return $this->deleteToken(strtolower($email));
        }
        public function crearToken($email,$selector,$token,$expire){
            return $this->createToken(strtolower($email),$selector,$token,$expire);
        }
        public function buscarToken($selector,$expire){
            return $this->selectToken($selector,$expire);
        }

        public function updatePersonal($nombre_p,$apellido_p,$rut_p,$cargo_p,$email_p,$telefono_p,$id_p){
            return $this->editPersonal($nombre_p,$apellido_p,$rut_p,$cargo_p,$email_p,$telefono_p,$id_p);
        }

        public function borrarPersonal($id_p){
            return $this->deletePersonal($id_p);
        }

    }

?>

