<?php
use PHPUnit\Framework\TestCase;
require_once dirname(__FILE__).'/../Dbh.class.php';
require_once dirname(__FILE__).'/../model/Clientes.class.php';
require_once dirname(__FILE__).'/../model/Reservas.class.php';
require_once dirname(__FILE__).'/../view/ReservasView.class.php';

    class ReservaTest extends TestCase {
    public function testlastMonday(){      
        $view = new ReservasView();  
        $fecha= '2021-01-20';
        $monto = $view->getlastMonday($fecha);
        $esperado = '2021-01-18';        
        $this->assertEquals($esperado, $monto);
        }
    }
?>