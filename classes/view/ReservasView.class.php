<?php
    require_once dirname(__FILE__).'/../view/ClientesView.class.php';     
    class ReservasView extends Reservas{
        public function verReserva($fecha,$hora){
            $resultado=$this->ReservasDiaHora($fecha,$hora);
            
            if($resultado===false){
                echo("<div class=\"cuadro\" style=\"background-color:#8ECAE6;\">");
                echo($hora);

            }
            else{
                $cliente=new ClientesView();
                echo("<div class=\"cuadro\" style=\"background-color:#FB8500;\">");
                echo("<p> id cliente:".$cliente->Nombrecliente($resultado["idcliente"])."</p>");
                echo("<p> servicio:".$resultado["nombreservicios"]."</p>");
            }
            echo("</div>");
        }
        public function VerReservasSemana($fecha){            
            echo("<div class='contenedor'>");
            $semana =$this->getlastMonday($fecha);  
            $diasemana=date('Y-m-d', strtotime($semana));         
            for ($dia=0; $dia < 7; $dia++){                
                for ($hora=10; $hora <= 20; $hora++){
                    $this->verReserva($diasemana,$hora);
                }
                $diasemana=date('Y-m-d', strtotime('+1 day',strtotime($diasemana )));
            }
            echo("</div>");
        }
        public function VerReservasSemana2($fecha){          //version nueva y ..mas rapida  
            setlocale(LC_TIME, "spanish");
            date_default_timezone_set('America/Santiago');
            $week = array("Lunes", "Martes", "Miercoles", "Jueves","Viernes","Sabado","Domingo");
            $cliente=new ClientesView();
            echo("<div class='contenedor'>");
                echo("<div class='tag'>Hora</div>");
                echo("<div class='tag'>10:00</div>");     
                echo("<div class='tag'>11:00</div>");
                echo("<div class='tag'>12:00</div>");
                echo("<div class='tag'>13:00</div>");
                echo("<div class='tag'>14:00</div>");
                echo("<div class='tag'>15:00</div>");
                echo("<div class='tag'>16:00</div>");     
                echo("<div class='tag'>17:00</div>"); 
                echo("<div class='tag'>18:00</div>");                
                echo("<div class='tag'>19:00</div>"); 
                echo("<div class='tag'>20:00</div>"); 
            $diareal=date('Y-m-d');
            $horareal=idate('H', time());
            $semana =$this->getlastMonday($fecha);  
            $diasemana=date('Y-m-d', strtotime($semana));         
            for ($dia=0; $dia < 7; $dia++){
                echo("<div class='tag'>".$week[$dia].",\n".strftime("%d de %B %Y", strtotime($diasemana))."</div>");
                $resultado=$this->ReservasDia($diasemana);                
                if($resultado->rowCount()==0){
                    for ($hora=10; $hora <= 20; $hora++){   
                        echo("<div class=\"cuadro\" style=\"background-color:#ECECEC;\">");
                        if(($diasemana == $diareal&&$hora >= $horareal)||($diasemana > $diareal)){                            
                                echo("<form action=\"agendarservicio.php\" method=\"post\">
                                <input type=\"hidden\" name=\"fechareserva\" value=\"".$diasemana."\">
                                <input type=\"hidden\" name=\"horareserva\" value=\"".$hora."\">
                                <button type=\"submit\" name=\"send\" class=\"botonreserva\" >Agendar reserva</button>
                                </form>");                            
                        }                        
                        echo("</div>");     
                        }
                }
                else{
                    $dato=$resultado->fetch();                    
                    for ($hora=10; $hora <= 20; $hora++){                            
                        if($dato!=NULL){
                            $horareserva=$dato['horareserva'];          
                            if($horareserva===$hora){
                                echo("<div class=\"cuadro\">");
                                echo("<div class=\"cuadroa\">");
                                echo("<p>Cliente:<br>".$cliente->Nombrecliente($dato["idcliente"])."</p>");
                                echo("<p>Servicio:".$dato["nombreservicios"]."</p>");
                                echo("</div>");
                                echo("</div>");
                                $dato=$resultado->fetch();
                            }                            
                            else{
                                echo("<div class=\"cuadro\" style=\"background-color:#ECECEC;\">");
                                if(($diasemana== $diareal&&$hora >= $horareal)||($diasemana > $diareal)){                            
                                    echo("<form action=\"agendarservicio.php\" method=\"post\">
                                    <input type=\"hidden\" name=\"fechareserva\" value=\"".$diasemana."\">
                                    <input type=\"hidden\" name=\"horareserva\" value=\"".$hora."\">
                                    <button type=\"submit\" name=\"send\" class=\"botonreserva\" >Agendar reserva</button>
                                    </form>");                                
                                }                        
                                echo("</div>");   
                            }
                        }
                        else{
                            echo("<div class=\"cuadro\" style=\"background-color:#ECECEC;\">");
                            if(($diasemana == $diareal&&$hora >= $horareal)||($diasemana > $diareal)){                            
                                echo("<form action=\"agendarservicio.php\" method=\"post\">
                                <input type=\"hidden\" name=\"fechareserva\" value=\"".$diasemana."\">
                                <input type=\"hidden\" name=\"horareserva\" value=\"".$hora."\">
                                <button type=\"submit\" name=\"send\" class=\"botonreserva\" >Agendar reserva</button>
                                </form>");                            
                            }                        
                            echo("</div>"); 
                        }                        
                    }                    
                }                
                $diasemana=date('Y-m-d', strtotime('+1 day',strtotime($diasemana )));
            }
            echo("</div>");
        }
        public function BuscadorReservaEliminar($buscar){
            return $this->BuscadorEliminarReserva($buscar);
        }
        public function VerBusquedaR($cliente){
            return $this-> VerBusqueda($cliente);
        }
        public function getlastMondayp($fecha){
            return $this->getlastMonday($fecha);
        }
      







        #esta funcion es utilizada en eliminarreserva.php
        public function VerReservasEliminar(){
            return $this->viewReservaEliminar();
        }
    }

?>
