<?php
    class Giftcard extends Dbh{

        protected function createGiftcard($fechahasta, $estado,$fechadesde, $idcliente, $nombreservicio){
            
            $conn = $this->connect();
            $stmt = $conn->prepare("INSERT INTO giftcards(fechahasta, estado, fechadesde, idcliente, servicio) VALUES (?, ?, ?, ?, ?);");
            $result = $stmt->execute([$fechahasta, $estado, $fechadesde, $idcliente, $nombreservicio]);
        }

        protected function estadoGC($estado){
                if($estado === false){
                    return $state = "no cobrada";
                }else{
                    return $state = "cobrada";
                }
        }

        protected function  viewGiftcards(){
            $conn = $this->connect();
            $stmt = $conn->prepare("SELECT * FROM giftcards ORDER BY fechadesde DESC");
            $stmt->execute();
            $data = $stmt->fetchALL();
            include_once "./classes/view/ClientesView.class.php";
            $cliente = new ClientesView();
            $estado = new Giftcard();
            echo("<table><tr> <th> Código Giftcard </th> <th> Cliente </th> <th> Servicio </th> <th> Fecha Inicio </th> <th> Fecha Caducidad </th> <th> Estado </th><th> Eliminar </th>");
            foreach($data as $row){
                echo("<tr>
                    <td>".$row['codigo']."</td>
                    <td>".$cliente->NombreCliente($row['idcliente'])."</td>
                    <td>".$row['servicio']."</td><td>".$row['fechadesde']."</td>
                    <td>".$row['fechahasta']."</td>
                    <td>".$estado->estadoGC($row['estado'])."
                
                <form action=\"./eliminargiftcard.php\" method=\"post\">
                    <input type=\"hidden\" name=\"codigo\" value=\"".$row['codigo']."\">
                    <input type=\"hidden\" name=\"idcliente\" value=\"".$row['idcliente']."\">
                    <input type=\"hidden\" name=\"servicio\" value=\"".$row['servicio']."\">
                    <input type=\"hidden\" name=\"fechadesde\" value=\"".$row['fechadesde']."\">
                    <input type=\"hidden\" name=\"fechahasta\" value=\"".$row['fechahasta']."\">
                    <input type=\"hidden\" name=\"estado\" value=\"".$row['estado']."\">
                    <input type=\"hidden\" name=\"idgiftcard\" value=\"".$row['idgiftcard']."\">
                    <td><button type=\"submit\" name=\"editar\" class=\"btn_deleteV\" >Eliminar</button>
                </form>
                </td></tr>

                ");
            }
            echo ("</table>");
        }

        protected function consultaGiftcards(){
            $conn = $this->connect();
            $stmt=$conn->prepare("SELECT * FROM giftcards WHERE estado='FALSE'");
            $stmt->execute();
            $data=$stmt->fetchAll();
            return $data;
        }

        protected function deleteGiftcard($idgiftcard){
            $conn = $this->connect();
            $stmt = $conn->prepare("DELETE FROM giftcards WHERE idgiftcard=?;");
            $stmt->execute([$idgiftcard]);
        }

    }
?>