<?php
    class Personal extends Dbh {

        protected function createPersonal($nombre,$apellido,$rut,$cargo,$email,$telefono,$pass){
            $conn = $this->connect();
            $hashPass = password_hash($pass,PASSWORD_DEFAULT);
            $stmt=$conn->prepare("INSERT INTO Personal(nombrepersonal,apellidopersonal,rutpersonal,cargopersonal,emailpersonal,telefonopersonal,clavepersonal) VALUES (?, ?, ?, ?, ?, ?, ?);");
            $result=$stmt->execute([$nombre,$apellido,$rut,$cargo,strtolower($email),$telefono,$hashPass]);
        }
        protected function userExist($email){
            $conn = $this->connect();
            $stmt=$conn->prepare("SELECT * FROM  Personal WHERE EmailPersonal = ?;");
            $stmt->execute([$email]);
            if( $result = $stmt->fetch()){
                return $result;}
            else{$result=false;
                return $result;}
        }
        protected function userUpdatePass($email,$pass){
            $hashPass = password_hash($pass,PASSWORD_DEFAULT);
            $conn = $this->connect();
            $stmt=$conn->prepare("UPDATE Personal SET ClavePersonal = ? WHERE EmailPersonal = ?;");
            $stmt->execute([$hashPass,$email]);
        }

        protected function deleteToken($email){
            $conn = $this->connect();
            $stmt=$conn->prepare("DELETE FROM  passreset WHERE EmailPersonal = ?;");
            $stmt->execute([$email]);
        }
        protected function createToken($email,$selector,$token,$expire){
            $conn = $this->connect();
            $stmt=$conn->prepare("INSERT INTO passreset(emailpersonal,resetselector,resettoken,resetexpire) VALUES (?, ?, ?, ?);");
            $result = $stmt->execute([strtolower($email),$selector,$token,$expire]);
        }
        protected function selectToken($selector,$expire){
            $conn = $this->connect();
            $stmt=$conn->prepare("SELECT * FROM  passreset WHERE resetselector = ? AND resetexpire>=?;");
            $stmt->execute([$selector,$expire]);
            if( $result = $stmt->fetch()){
                return $result;}
            else{$result=false;
                return $result;}
        }
        protected function viewPersonal(){
            $conn = $this->connect();
            $stmt = $conn->prepare("SELECT * FROM Personal ORDER BY nombrepersonal DESC");
            $stmt->execute();
            $data = $stmt->fetchALL();
            echo("<table><tr><th>Nombres</th><th>Apellidos</th><th>Rut</th><th>Cargo</th><th>Email </th><th>Telefono</th><th>Modificar</th></tr>");
            foreach($data as $row){
                echo("<tr><td>".$row['nombrepersonal']."</td><td>".$row['apellidopersonal']."</td><td>".$row['rutpersonal']."</td><td>".$row['cargopersonal']."</td><td>".$row['emailpersonal']."</td><td>".$row['telefonopersonal']."
                <form action=\"./editarpersonal.php\" method=\"post\">
                <input type=\"hidden\" name=\"nombrepersonal\" value=\"".$row['nombrepersonal']."\">
                <input type=\"hidden\" name=\"apellidopersonal\"value=\"".$row['apellidopersonal'] ."\">
                <input type=\"hidden\" name=\"rutpersonal\" value= ".$row['rutpersonal'] .">
                <input type=\"hidden\" name=\"cargopersonal\" value=".$row['cargopersonal'] .">
                <input type=\"hidden\" name=\"emailpersonal\" value=".$row['emailpersonal'].">
                <input type=\"hidden\" name=\"telefonopersonal\" value=".$row['telefonopersonal'].">
                <input type=\"hidden\" name=\"idpersonal\" value=".$row['idpersonal']." >
                <td><button type=\"submit\" name=\"editar\" class=\"btneditar\" >Modificar</button>
                </form>
                <form action=\"./eliminarPersonal.php\" method=\"post\">
                <input type=\"hidden\" name=\"nombrepersonal\" value=\"".$row['nombrepersonal']."\">
                <input type=\"hidden\" name=\"apellidopersonal\"value=\"".$row['apellidopersonal'] ."\">
                <input type=\"hidden\" name=\"rutpersonal\" value= ".$row['rutpersonal'] .">
                <input type=\"hidden\" name=\"cargopersonal\" value=".$row['cargopersonal'] .">
                <input type=\"hidden\" name=\"emailpersonal\" value=".$row['emailpersonal'].">
                <input type=\"hidden\" name=\"telefonopersonal\" value=".$row['telefonopersonal'].">
                <input type=\"hidden\" name=\"idpersonal\" value=".$row['idpersonal']." >
                <td><button type=\"submit\" name=\"editar\" class=\"btn_deleteV\" >Eliminar</button>
                </form>
                </td></tr>");
            }
            echo ("</table>");
        }

        protected function editPersonal($nombre_p, $apellido_p, $rut_p, $cargo_p, $email_p, $telefono_p, $id_p){
            $conn = $this->connect();
            $stmt = $conn->prepare("UPDATE personal SET nombrepersonal=?, apellidopersonal=?, rutpersonal=?, cargopersonal=?, emailpersonal=?, telefonopersonal=? WHERE idpersonal=?;");
            $result = $stmt->execute([$nombre_p, $apellido_p, $rut_p, $cargo_p, strtolower($email_p), $telefono_p,$id_p]);
        }

        protected function VerBusqueda($personal){
            echo("<table><tr><th>Nombres</th><th>Apellidos</th><th>Rut</th><th>Telefono</th><th>Cargo</th><th>Editar</th></tr>");
            foreach($personal as $row) {
                echo("<tr><td>".$row['nombrepersonal']."</td><td>".$row['apellidopersonal']."</td><td>".$row['rutpersonal']."</td><td>".$row['cargopersonal']."</td><td>".$row['emailpersonal']."</td>
                <form action=\"./editarpersonal.php\" method=\"post\">
                <input type=\"hidden\" name=\"nombrepersonal\" value=".$row['nombrepersonal'].">
                <input type=\"hidden\" name=\"apellidopersonal\"value=".$row['apellidopersonal'] .">
                <input type=\"hidden\" name=\"rutpersonal\" value= ".$row['rutpersonal'] .">
                <input type=\"hidden\" name=\"cargopersonal\" value=".$row['cargopersonal'] .">
                <input type=\"hidden\" name=\"emailpersonal\" value=".$row['emailpersonal'].">
                <input type=\"hidden\" name=\"telefonopersonal\" value=".$row['telefonopersonal'].">
                <td><button type=\"submit\" name=\"editar\" class=\"btneditar\" >Editar</button>
                </form>
                <form action=\"./eliminarPersonal.php\" method=\"post\">
                <input type=\"hidden\" name=\"nombrepersonal\" value=\"".$row['nombrepersonal']."\">
                <input type=\"hidden\" name=\"apellidopersonal\"value=\"".$row['apellidopersonal'] ."\">
                <input type=\"hidden\" name=\"rutpersonal\" value= ".$row['rutpersonal'] .">
                <input type=\"hidden\" name=\"cargopersonal\" value=".$row['cargopersonal'] .">
                <input type=\"hidden\" name=\"emailpersonal\" value=".$row['emailpersonal'].">
                <input type=\"hidden\" name=\"telefonopersonal\" value=".$row['telefonopersonal'].">
                <input type=\"hidden\" name=\"idpersonal\" value=".$row['idpersonal']." >
                <td><button type=\"submit\" name=\"editar\" class=\"btneditar\" >Eliminar</button>
                </form></td></tr>");

            }
            echo("</table>");

        }
        protected function BuscadorpersonalR($nombre){
            $conn = $this->connect();
            $stmt=$conn->prepare("SELECT * FROM  Personal WHERE LOWER (nombrepersonal) LIKE ? OR LOWER (apellidopersonal) LIKE ? OR LOWER (rutpersonal) LIKE ? OR LOWER (emailpersonal) LIKE ?; ");
            $stmt->execute([$nombre, $nombre, $nombre, $nombre]);
            if( $result = $stmt->fetchAll()){
                return $result;}
            else{$result=false;
                return $result;}
        }
        protected function deletePersonal($id_p){
            $conn = $this->connect();
            $stmt = $conn->prepare("DELETE FROM Personal WHERE idpersonal=?;");
            $stmt->execute([$id_p]);
        }

    }

?>

