<?php
    class Reservas extends Dbh {
        protected function getlastMonday($fecha){
            $cadena=date('l',strtotime($fecha));           
            if(strcmp($cadena,"Sunday")==0 || strcmp($cadena,"Domingo")==0){
                $dia = strtotime('last monday',strtotime($fecha));
                return date('Y-m-d', $dia);
            }
            $dia = strtotime('tomorrow',strtotime($fecha));
            $dia = strtotime('last monday',$dia);
            return date('Y-m-d', $dia);
        }
        protected function ReservasDia($fecha){
            $conn = $this->connect();
            $stmt = $conn->prepare("SELECT * FROM Reservas where fechareserva = ? ORDER BY horareserva;");
            $stmt->execute([$fecha]);
            return $stmt;
        }
        protected function ReservasDiaHora($fecha,$hora){
            $conn = $this->connect();
            $stmt = $conn->prepare("SELECT * FROM Reservas where fechareserva = ? AND horareserva=?;");
            $stmt->execute([$fecha,$hora]);
            if( $result = $stmt->fetch()){
                return $result;}
            else{$result=false;
                return $result;}
        }
        protected function createReserva($idcliente,$nombreservicios,$fechareserva,$horareserva,$idgiftcard){
            $conn = $this->connect();
            $stmt=$conn->prepare("INSERT INTO Reservas(idcliente, nombreservicios, fechareserva, horareserva, idgiftcard) VALUES (?, ?, ?, ?, ?);");
            $result = $stmt->execute([$idcliente,$nombreservicios,$fechareserva,$horareserva,$idgiftcard]);
        }

        #Ver las reservas registradas de la fecha actual en adelante
        protected function viewReservaEliminar(){
            $conn = $this->connect();
            $stmt=$conn->prepare("SELECT * FROM reservas INNER JOIN clientes ON reservas.idcliente = clientes.idcliente  WHERE fechareserva >= CURRENT_DATE ORDER BY fechareserva,horareserva ASC");
            $stmt->execute();
            $data=$stmt->fetchAll();
            echo"<table><tr><th>Fecha</th><th>Hora</th><th>Servicio</th><th>Nombre</th><th>Apellido</th><th>Rut</th><th>Reserva</th></tr>";
            foreach($data as $row) {
            echo("<tr>
            <td>".$row['fechareserva']."</td>
            <td>".$row['horareserva']."</td>
            <td>".$row['nombreservicios']."</td>
            <td>".$row['nombrescliente']."</td>
            <td>".$row['apellidoscliente']."</td>
            <td>".$row['rutcliente']."</td>

                <form action=\"./eliminarR.php\" method=\"post\">
                <input type=\"hidden\" name=\"fechareserva\" value=\"".$row['fechareserva']."\">
                <input type=\"hidden\" name=\"horareserva\"value=\"".$row['horareserva'] ."\">
                <input type=\"hidden\" name=\"nombreservicios\" value= ".$row['nombreservicios'] ."\">
                <input type=\"hidden\" name=\"nombrescliente\"value=\"".$row['nombrescliente'] ."\">
                <input type=\"hidden\" name=\"apellidoscliente\" value= ".$row['apellidoscliente'] ."\">
                <input type=\"hidden\" name=\"rutcliente\" value=\"".$row['rutcliente']."\">
                <input type=\"hidden\" name=\"idreserva\" value=\"".$row['idreserva']."\">
                <td><button type=\"submit\" name=\"eliminar\" class=\"btn_deleteV\" >Anular</button>
                </form></td></tr>
                ");
            } 
            echo"</table>";            
        }

        public function BuscadorEliminarReserva($buscar){
            $conn = $this->connect();
            $stmt=$conn->prepare("SELECT * FROM reservas INNER JOIN clientes ON reservas.idcliente = clientes.idcliente WHERE fechareserva >= CURRENT_DATE AND LOWER (nombrescliente) LIKE ? OR LOWER (apellidoscliente) LIKE ? OR LOWER (rutcliente) LIKE ? ;");
            $stmt->execute([$buscar, $buscar, $buscar]);
            if($result = $stmt->fetchAll()){
                return $result;}
            else{$result=false;
                return $result;
            }
        }

        public function VerBusqueda($cliente){
            echo"<table><tr><th>Fecha</th><th>Hora</th><th>Servicio</th><th>Nombre</th><th>Apellido</th><th>Rut</th><th>Reserva</th></tr>";      
            foreach($cliente as $row){
            echo("<tr>
            <td>".$row['fechareserva']."</td>
            <td>".$row['horareserva']."</td>
            <td>".$row['nombreservicios']."</td>
            <td>".$row['nombrescliente']."</td>
            <td>".$row['apellidoscliente']."</td>
            <td>".$row['rutcliente']."</td>
                <form action=\"./eliminarR.php\" method=\"post\">
                <input type=\"hidden\" name=\"fechareserva\" value=\"".$row['fechareserva']."\">
                <input type=\"hidden\" name=\"horareserva\"value=\"".$row['horareserva'] ."\">
                <input type=\"hidden\" name=\"nombreservicios\" value= ".$row['nombreservicios'] ."\">
                <input type=\"hidden\" name=\"nombrescliente\"value=\"".$row['nombrescliente'] ."\">
                <input type=\"hidden\" name=\"apellidoscliente\" value= ".$row['apellidoscliente'] ."\">
                <input type=\"hidden\" name=\"rutcliente\" value=\"".$row['rutcliente']."\">
                <input type=\"hidden\" name=\"idreserva\" value=\"".$row['idreserva']."\">
                <td><button type=\"submit\" name=\"eliminar\" class=\"btn_deleteV\" >Anular</button>
                </form></td></tr>
                ");                
            }    
            echo"</table>";
        }
        protected function Reservaborrar($idreserva){
        $conn = $this->connect();
        $stmt=$conn->prepare("DELETE FROM  reservas  WHERE idreserva = ?;");
        $stmt->execute([$idreserva]);     
        }
        protected function consultaReservas(){
            $conn = $this->connect();
            $stmt=$conn->prepare("SELECT * FROM reservas ORDER BY idreserva DESC");
            $stmt->execute();
            $data=$stmt->fetchAll();
            return $data;
        }
    }

?>