<?php     
    class Servicio extends Dbh {
        //creacion del Servicio
        //esta funcion es llamada desde ServicioContr
        protected function createServicios($nombreServicio,$descripcionServicio,$preciosservicios){
            $conn = $this->connect();
            $stmt=$conn->prepare("INSERT INTO servicio(nombreservicios,descripcionservicios,preciosservicios) VALUES (?, ?,?);");
            $result = $stmt->execute([$nombreServicio,$descripcionServicio,$preciosservicios]);
        }
         
        //Editar Servicio
        //esta clase es llamada desde las  ServicioContr
        protected function editServicio($s_nombre,$s_descripcion,$s_precio){
            $conn = $this->connect();
            $stmt=$conn->prepare("UPDATE servicio SET nombreservicios =?, descripcionservicios =?, preciosservicios =? WHERE nombreservicios =?;");
            $result = $stmt->execute([$s_nombre,$s_descripcion,$s_precio,$s_nombre]);
        }
         //eliminar servicio 
       //esta funcion es llamada desde ServicioContr
       protected function deleteServicio($nombreServicio){
        $conn = $this->connect();
        $stmt=$conn->prepare("DELETE FROM  servicio  WHERE nombreservicios = ?;");
        $stmt->execute([$nombreServicio]);       
    }
    #Buscar los servicio 
    #esta funcion es llamada desde ServicioView 
    protected function BuscadorServicioNombre($nombreServicio){
        $conn = $this->connect();
        $stmt=$conn->prepare("SELECT * FROM  servicio WHERE nombreservicios LIKE ? ;");
        $stmt->execute([$nombreServicio]);           
        if( $result = $stmt->fetchAll()){                
            return $result;
        }
        else{$result=false;
            return $result;
        }
    }
     #lista los servicio 
    #esta funcion es llamada desde ServicioView 
    protected function VerbusquedaS($servicio){
        echo"<table><tr><th>Nombres Servicio</th><th>Descripcion Servicio</th><th>PrecioServicio</th></tr>";              
        foreach($servicio as $row) {                
            echo"<tr>
            <td>".$row['nombreservicios']."</td>
            <td>".$row['descripcionservicios']."</td>
            <td>".$row['preciosservicios']."</td>
            <form >
            <input type=\"hidden\" name=\"nombreservicios\" value=".$row['nombreservicios'].">
            <input type=\"hidden\" name=\"descripcionservicios\"value=".$row['descripcionservicios'] .">
            <input type=\"hidden\" name=\"preciosservicios\" value= ".$row['preciosservicios'] .">
            
            </form></td></tr>
            ";       

        }  
        echo"</table>";  
        
    }  

       # ver los servicios
       #esta funcion es llamada desde ServicioView 
       protected function viewServicio(){
        $conn = $this->connect();
        $stmt=$conn->prepare("SELECT * FROM servicio ORDER BY nombreservicios DESC");
        $stmt->execute();           
        $data=$stmt->fetchAll();
        echo"<table><tr><th>Nombre Servicio</th><th>Descripcion Servicio</th><th>Precio Servicio</th><th>Editar</th></tr>";              
        foreach($data as $row) {                
        echo"<tr>
        <td>".$row['nombreservicios']."</td>
        <td>".$row['descripcionservicios']."</td>
        <td>".$row['preciosservicios']."</td>
            <form action=\"./editarservicio.php\" method=\"post\">
            <input type=\"hidden\" name=\"nombreservicio\" value=\"".$row['nombreservicios']."\">
            <input type=\"hidden\" name=\"descripcion_servicio\"value=\"".$row['descripcionservicios'] ."\">
            <input type=\"hidden\" name=\"precio_servicio\" value= ".$row['preciosservicios'] .">
            <td><button type=\"submit\" name=\"editar\" class=\"btneditar\" >Editar</button>
                                   
            </form>
                           
            </td>
            
            </tr>";
        } 

      echo"</table>";            
    }
    
    protected function consultaServicios(){
        $conn = $this->connect();
        $stmt=$conn->prepare("SELECT * FROM servicio ORDER BY nombreservicios DESC");
        $stmt->execute();           
        $data=$stmt->fetchAll();     
        return $data;      
    }
    
       
          #ver servicio a eliminar 
          protected function viewServicioEliminar(){
            $conn = $this->connect();
            $stmt=$conn->prepare("SELECT * FROM servicio ORDER BY nombreservicios DESC");
            $stmt->execute();           
            $data=$stmt->fetchAll();
            echo"<table><tr><th>Nombre Servicio</th><th>Descripcion Servicio</th><th>Precio Servicio</th><th>Eliminar</th></tr>";              
            foreach($data as $row) {                
            echo("<tr>
            <td>".$row['nombreservicios']."</td>
            <td>".$row['descripcionservicios']."</td>
             <td>".$row['preciosservicios']."</td>
                <form action=\"./eliminarservicio.php\" method=\"post\">
                <input type=\"hidden\" name=\"nombreservicioEliminar\" value=\"".$row['nombreservicios']."\">
                <input type=\"hidden\" name=\"descripcion_servicio\"value=\"".$row['descripcionservicios'] ."\">
                <input type=\"hidden\" name=\"precio_servicio\" value= ".$row['preciosservicios'] .">
                <td><button type=\"submit\" name=\"eliminar\" class=\"btneliminar\" >Eliminar</button>
                                       
                </form>
                               
                </td>
                
                </tr>");
            } 

          echo"</table>";            
        }  
        
       
        
        

    } 
    
?>

