<?php
    class Clientes extends Dbh {

        protected function createCliente($c_nombres,$c_apellidos,$c_rut,$c_email,$c_telefono,$c_tipo,$c_comentario){
            $conn = $this->connect();
            $stmt=$conn->prepare("INSERT INTO Clientes(nombrescliente,apellidoscliente,rutcliente,emailcliente,telcliente,tipocliente,comentariocliente) VALUES (?, ?, ?, ?, ?, ?, ?);");
            $result = $stmt->execute([$c_nombres,$c_apellidos,$c_rut,strtolower($c_email),$c_telefono,$c_tipo,$c_comentario]);
        }
        protected function clienteExist($c_email){
            $conn = $this->connect();
            $stmt=$conn->prepare("SELECT * FROM  Clientes WHERE emailcliente = ?;");
            $stmt->execute([$c_email]);
            if( $result = $stmt->fetch()){                
                return $result;}
            else{$result=false;
                return $result;}
        }
        protected function Namecliente($idcliente){
            $conn = $this->connect();
            $stmt=$conn->prepare("SELECT * FROM  Clientes WHERE idcliente = ?;");
            $stmt->execute([$idcliente]);
            if( $result = $stmt->fetch()){                
                return $result["nombrescliente"]." ".$result["apellidoscliente"];}
            else{$result=false;
                return $result;}
        }

        protected function viewClientes(){
            $conn = $this->connect();
            $stmt=$conn->prepare("SELECT * FROM  Clientes ORDER BY nombrescliente DESC");
            $stmt->execute();           
            $data=$stmt->fetchAll();    
            echo("<table><tr><th>Nombres</th><th>Apellidos</th><th>Run</th><th>Telefono</th><th>Email</th><th>Tipo</th><th>Comentario</th><th>Editar</th></tr>");              
            foreach($data as $row) {                
                echo("<tr><td>".$row['nombrescliente']."</td><td>".$row['apellidoscliente']."</td><td>".$row['rutcliente']."</td><td>".$row['telcliente']."</td><td>".$row['emailcliente']."</td><td>".$row['tipocliente']."</td><td>".$row['comentariocliente']."
                </td>
                
                <form action=\"./editarcliente.php\" method=\"post\">
                <input type=\"hidden\" name=\"nombrescliente\" value=\"".$row['nombrescliente']."\">
                <input type=\"hidden\" name=\"apellidoscliente\"value=\"".$row['apellidoscliente'] ."\">
                <input type=\"hidden\" name=\"rutcliente\" value= ".$row['rutcliente'] .">
                <input type=\"hidden\" name=\"emailcliente\" value=".$row['emailcliente'] .">
                <input type=\"hidden\" name=\"telcliente\" value=".$row['telcliente'].">
                <input type=\"hidden\" name=\"tipocliente\" value=".$row['tipocliente'].">
                <input type=\"hidden\" name=\"comentariocliente\" value=\"".$row['comentariocliente']."\">
                <input type=\"hidden\" name=\"idcliente\" value= ".$row['idcliente'].">
                <td><button type=\"submit\" name=\"editar\" class=\"btneditar\" >Editar</button>
                </form></td></tr>
                ");    
            }  
            echo("</table>");  
        }

        protected function consultaClientes(){
            $conn = $this->connect();
            $stmt=$conn->prepare("SELECT * FROM clientes ORDER BY nombrescliente DESC");
            $stmt->execute();           
            $data=$stmt->fetchAll();     
            return $data;
        }

        protected function editCliente($c_nombres,$c_apellidos,$c_rut,$c_email,$c_telefono,$c_tipo,$c_comentario,$idcliente){
            $conn = $this->connect();
            $stmt=$conn->prepare("UPDATE Clientes SET nombrescliente = ?, apellidoscliente = ?, rutcliente = ?, emailcliente=?,telcliente=?,tipocliente=?,comentariocliente=? WHERE idcliente = ?;");
            $result = $stmt->execute([$c_nombres,$c_apellidos,$c_rut,strtolower($c_email),$c_telefono,$c_tipo,$c_comentario,$idcliente]);
        }

        protected function Buscadorclientenombre($nombre){
            $conn = $this->connect();
            $stmt=$conn->prepare("SELECT * FROM  clientes WHERE LOWER (nombrescliente) LIKE ? OR LOWER (apellidoscliente) LIKE ? OR LOWER (rutcliente) LIKE ? OR LOWER (emailcliente) LIKE ?  ; ");
            $stmt->execute([$nombre, $nombre, $nombre, $nombre]);           
            if( $result = $stmt->fetchAll()){                
                return $result;}
            else{$result=false;
                return $result;}
        }

        protected function Verbusqueda($cliente){
            echo("<table><tr><th>Nombres</th><th>Apellidos</th><th>Run</th><th>Telefono</th><th>Email</th><th>Tipo</th><th>Comentario</th><th>Editar</th></tr>");              
            foreach($cliente as $row) {                
                echo("<tr><td>".$row['nombrescliente']."</td><td>".$row['apellidoscliente']."</td><td>".$row['rutcliente']."</td><td>".$row['telcliente']."</td><td>".$row['emailcliente']."</td><td>".$row['tipocliente']."</td><td>".$row['comentariocliente']."
                </td>
                
                <form action=\"./editarcliente.php\" method=\"post\">
                <input type=\"hidden\" name=\"nombrescliente\" value=".$row['nombrescliente'].">
                <input type=\"hidden\" name=\"apellidoscliente\"value=".$row['apellidoscliente'] .">
                <input type=\"hidden\" name=\"rutcliente\" value= ".$row['rutcliente'] .">
                <input type=\"hidden\" name=\"emailcliente\" value=".$row['emailcliente'] .">
                <input type=\"hidden\" name=\"telcliente\" value=".$row['telcliente'].">
                <input type=\"hidden\" name=\"tipocliente\" value=".$row['tipocliente'].">
                <input type=\"hidden\" name=\"comentariocliente\" value=\"".$row['comentariocliente']."\">
                <input type=\"hidden\" name=\"idcliente\" value= ".$row['idcliente'].">
                <td><button type=\"submit\" name=\"editar\" class=\"btneditar\" >Editar</button>
                </form></td></tr>
                ");
            }  
            echo("</table>");  
            
        }
    }

?>

