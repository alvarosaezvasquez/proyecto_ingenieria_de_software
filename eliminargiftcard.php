<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Eliminar Giftcard | EasySpa</title>
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="css/formulariopass.css">
    <link rel="stylesheet" href="css/listado.css">
    <link rel="stylesheet" href="css/formulario.css">
    <link rel="stylesheet" href="css/alpha.css">

</head>

<body>
    <?php include "./include/header.php" ?>
    
    <?php
    
    $cliente = new ClientesView();

    $idcliente = $_POST['idcliente'];
    $client = $cliente->NombreCliente($idcliente);
    $codigo = $_POST['codigo'];
    $fechadesde = $_POST['fechadesde'];
    $fechahasta = $_POST['fechahasta'];
    $estado = $_POST['estado'];
    $servicio = $_POST['servicio'];
    $idgiftcard = $_POST['idgiftcard'];


    echo ("
        <div class=\"edit_p\">
            <br>
            <h2>Usted va a eliminar la siguiente giftcard:</h2>
            <br>
            <form action=\"./include/eliminargiftcard2.php\" method=\"post\">
                <input type=\"number\" name=\"codigo\" placeholder=\"codigo\" value=\"".$codigo."\" required=\"required\" readonly=\"readonly\">
                <input type=\"text\" name=\"idcliente\" placeholder=\"cliente\" value=\"".$client."\" required=\"required\" readonly=\"readonly\">
                <input type=\"text\" name=\"servicio\" placeholder=\"servicio\" value=\"".$servicio."\" required=\"required\" readonly=\"readonly\">
                <input type=\"date\" name=\"fechadesde\" placeholder=\"fecha inicio\" value=\"".$fechadesde."\" required=\"required\" readonly=\"readonly\">
                <input type=\"date\" name=\"fechahasta\" placeholder=\"fecha hasta\" value=\"".$fechahasta."\" required=\"required\" readonly=\"readonly\">
                <input type=\"hidden\" name=\"idgiftcard\" placeholder=\"idgiftcard\" value=\"".$idgiftcard."\" required=\"required\" readonly=\"readonly\">

                <button type=\"submit\" name=\"delete\" class=\"btn_delete\" >Confirmar</button>
            </form>
            <br>
            <br>
            <form class=\"btn_cancel\" action=\"./giftcard.php\"\" method=\"post\">
                <button type=\"submit\" name=\"cancelar\" class=\"btn\" >Cancelar</button>
            </form>
        </div>"
    );

    ?>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <?php include "./include/footer.php" ?>



</body>

</html> 