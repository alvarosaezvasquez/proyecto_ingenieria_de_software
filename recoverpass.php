<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Recuperar contraseña | EasySpa</title>   
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="css/formulariopass.css">
</head>

<body>
    <?php include "./include/header.php" ?>     
    <?php     
    $status=0;
    if(isset($_SESSION["idpersonal"])){       
       header("location: ./index.php");
       exit();
    }
    else{
        if(isset($_GET["status"])){
            if($_GET["status"]== "passnocoincide"){               
             echo("<p>contraseñas no coinciden, intente de nuevo</p>");            
            }  
            if($_GET["status"]== "noexiste"){
                $status=1;
                echo("<p>correo no valido</p>");            
            }
            if($_GET["status"]== "tiempoexpirado"){
                $status=1;
                echo("<p>se acabo el tiempo permitido, intente de nuevo</p>");            
            }
            if($_GET["status"]== "validacionincorrecta"){
                $status=1;
                echo("<p>No altere el enlace de recupeecion!</p>");            
            }
        } 
        if(isset($_POST["forgot"])||$status===1){                                     
            echo("<div class=\"login\">");
            echo("<form action=\"./include/recoverpass2.php\" method=\"post\">");
    	    echo("<input type=\"text\" name=\"user\" placeholder=\"Correo Electronico\" required=\"required\" />");        
            echo("<button type=\"submit\" name=\"forgotsent\" class=\"btn\" >Enviar correo de recuperacion</button>");
            echo("</form>");
            echo("</div>");
        }
        else if(isset($_GET["selector"])&& isset($_GET["validator"])){            
            $selector=$_GET["selector"];
            $validator=$_GET["validator"];
            if(ctype_xdigit($selector)===true && ctype_xdigit($validator)===true){
                echo("<div class=\"login\">");
                echo("<form action=\"./include/recoverpass2.php\" method=\"post\">");
                echo("<input type=\"hidden\" name=\"selector\" value=\"".$selector."\" />");
                echo("<input type=\"hidden\" name=\"validator\" value=\"".$validator."\" />");       
                echo("<input type=\"password\" name=\"pass\" placeholder=\"Escriba su nueva Contraseña\" required=\"required\" />");  
                echo("<input type=\"password\" name=\"passrepeat\" placeholder=\"Repita su nueva Contraseña\" required=\"required\" />");   
                echo("<button type=\"submit\" name=\"changepass\" class=\"btn\" >Cambiar Contraseña</button>");
                echo("</form>");
                echo("</div>");
            }
        }
        else{
           header("location: ./index.php");
           exit();
        }       
    }          
    
    ?>   
    
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <?php include "./include/footer.php" ?>
    
</body>
</html>