<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Personal | EasySpa</title>
    <link rel="stylesheet" href="css/alpha.css">

</head>

<body>
    <?php include "./include/header.php" ?>
    <?php include "./classes/model/Personal.class.php" ?>

    <section class= "contenedor">
    <h1>Lista del Personal</h1>
    <div>
        <form action="./registrarpersonal.php" class="form_register" >
            <input type="submit" value="Nuevo Personal" class="btn_registerP">
        </form>
        <form action="./buscadorpersonal.php" class="form_search" method="POST" >
            <input type="text" name="busqueda" id="busqueda" placeholder="Buscar personal..." class ="bcliente">
            <input type="submit" value="Buscar" class="btn_search">
        </form>
        <br>
    </div>
    </section>
    <?php
    if(isset($_GET["status"])){
        if($_GET["status"]== "personalRegistrado"){
            echo("<p class=\"alert\">Personal registrado</p>");
        }
        if($_GET["status"]== "personalModificado"){
            echo("<p class=\"alert\">Personal modificado correctamente</p>");
        }
        if($_GET["status"] == "eliminado"){
            echo("<p class=\"alert\">Personal eliminado correctamente</p>");
        }
    }
    ?>

    <?php
    $personal = new PersonalView();
    $personal->VerPersonal();

    ?>

    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <?php include "./include/footer.php" ?>

</body>

</html>