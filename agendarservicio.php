<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Agendar reserva | EasySpa</title>
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="css/formulariopass.css">
    <link rel="stylesheet" href="css/formulario.css">
</head>
<body>
    <?php
        include "./include/header.php";
        include_once "./classes/view/ServicioView.class.php";
        include_once "./classes/view/ClientesView.class.php";
        include_once "./classes/view/GiftcardView.class.php";
        $servicios= new ServicioView();    
        $nombreserv=$servicios->consultarServicios();
        $clientes= new ClientesView();    
        $rutcli=$clientes->consultarClientes();
        $giftcards= new GiftcardView();
        $giftcard=$giftcards->consultarGiftcards();
        if(isset($_GET["status"])){
            if($_GET["status"]== "fechainvalida"){
                echo("<p>Fecha u hora inválida</p>");
            }
            if($_GET["status"]== "reservaexiste"){
                echo("<p>Ya existe una reserva para fecha y hora ingresada</p>");
            }
            if($_GET["status"]== "giftcardincorrecta"){
                echo("<p>La giftcard ingresada no es correcta</p>");
            }
        }
    ?>
    <section id="container">
        <div class="form_register">
            <h1>Agendar Reserva</h1>
            <form action="./include/agendarservicio2.php" method="post">
            Cliente:
            <select name="idcliente" required="required">
                <?php foreach ($rutcli as $r1): 
                echo("<option value=\"".$r1['idcliente']."\" >".$r1['nombrescliente']." ".$r1['apellidoscliente']."</option>");
                endforeach; ?>
            </select>
            <br>
            Servicio:
            <select name="nombreservicios" required="required">
                <?php foreach ($nombreserv as $r): 
                echo("<option value=\"".$r['nombreservicios']."\" >".$r['nombreservicios']."</option>");
                endforeach; ?>
            </select>
            <div>  <p>Fecha:</p> </div>
            
            <input type="date" name="fechareserva" placeholder="Fecha" value ="<?php 
            if(isset($_POST["fechareserva"]))
            {echo($_POST["fechareserva"]);}         
            ?>" required="required"/>
            Hora:
            <select name="horareserva"  required="required"/>
            <?php 
            if(isset($_POST["horareserva"])){
                for ($i=10; $i <=20 ; $i++) { 
                   echo("<option value='".$i."'");
                    if($i==intval($_POST["horareserva"])){echo(" selected");}
                   echo(">".$i."</option>");
                }
            }
            else {
                for ($i=10; $i <=20 ; $i++) { 
                    echo("<option value='".$i."'>".$i."</option>");
                 }
            }        
            ?>
            </select>
            <br>
            Giftcard (Seleccione 0 si no utiliza):
            <select name="idgiftcard" required="required">
                <option>0</option>
                <?php foreach ($giftcard as $r2): 
                echo("<option value='".$r2['idgiftcard']."' >".$r2['idgiftcard']."</option>");
                endforeach; ?>
            </select>
            <br><br>
            <button type="submit" name="agendar" class="btn" >Agendar</button>
            </form>
        </div>
    </section>
    
    <?php include "./include/footer.php" ?>

</body>

</html>