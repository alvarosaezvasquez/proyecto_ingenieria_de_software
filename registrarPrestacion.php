<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Crear Prestacion | EasySpa</title>
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="css/formulariopass.css">
    <link rel="stylesheet" href="css/formulario.css">
</head>
<body>

    <?php
        include "./include/header.php";
        include_once "./classes/view/ServicioView.class.php";
        include_once "./classes/view/ClientesView.class.php";
        include_once "./classes/view/PrestacionesView.class.php";

        $servicios= new ServicioView();    
        $nombreserv=$servicios->consultarServicios();

        $clientes= new ClientesView();    
        $rutcli=$clientes->consultarClientes();

        $reserva=new PrestacionesView();
        $idreserva=$reserva->Muestrareserva();

        if(isset($_GET["status"])){
            if($_GET["status"]== "fechainvalida"){
                echo("<p>Fecha de inicio o de caducidad inválida, favor de revisar las fechas</p>");
            }
        }
    ?>

    <br>

    <section id="container">
        <div class="form_register">

            <h1>Creación de Prestacion</h1><br>
            <form action="./include/nuevagiftcard2.php" method="post">

                <div><p>Cliente:</p></div>

                <select name="idcliente" required="required">
                    <?php foreach ($rutcli as $r1): 
                    echo("<option value=\"".$r1['idcliente']."\" >".$r1['nombrescliente']." ".$r1['apellidoscliente']."</option>");
                    endforeach; ?>
                </select>

                <br>
                <br>

                <div><p>Servicio</p></div>

                <select name="nombreservicios" required="required">
                    <?php foreach ($nombreserv as $r): 
                    echo("<option value=\"".$r['nombreservicios']."\" >".$r['nombreservicios']."</option>");
                    endforeach; ?>
                </select>

                <br>
                <br>
                <div><p>Ingrese la Fecha:</p></div>
                
                <input type="date" name="fechadesde" placeholder="Fecha de inicio" value ="<?php 
                if(isset($_POST["fechadesde"]))
                {echo($_POST["fechadesde"]);}         
                ?>" required="required"/>
                <br>
                <br>
                
                <div><p>Hora</p></div>

                <select name="nombreservicios" required="required">
                <?php foreach ($nombreserv as $r): 
                echo("<option value=\"".$r['nombreservicios']."\" >".$r['nombreservicios']."</option>");
                endforeach; ?>
                </select>

                <br>
                <br>
                

                

                <button type="submit" name="crear" class="btn" >Crear</button>

            </form>
        </div>
    </section>
    
    <br><br><br><br>
    <br><br><br><br>
    <br><br><br><br>
    <br><br><br><br>
    <br>
    <?php include "./include/footer.php" ?>

</body>

</html>