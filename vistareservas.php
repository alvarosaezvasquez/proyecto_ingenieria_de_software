<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Vista reservas | EasySpa</title>
    <link rel="stylesheet" href="css/vistareservas.css">
    <link rel="stylesheet" href="css/listado.css">
   

</head>

<body>
    <?php include_once "./include/header.php"?>
    <?php
    if(isset($_GET["status"])){
        if($_GET["status"]== "agendado"){
            echo("<p>Servicio agendado exitosamente</p>");
        }
    }
    ?>
    <section class ="container">
    <h1>Calendarizacion</h1>
    <div class="barrafecha">        
        <div>
            <form action="vistareservas.php" method="post">
                <input type="hidden" name="date" 
                <?php 
                    if(isset($_POST["date"])){        
                        $antes=date('Y-m-d',strtotime("last week",strtotime($_POST["date"]))); }
                    else{$antes=date('Y-m-d', strtotime('last week')); }
                echo("value='".$antes."'")?>>
                <button type="submit" class="fechaa" >Semana anterior</button>
            </form>
        </div>
        <div class="fecha" >
            <form action="vistareservas.php" method="post">
                <input type="date" name="date" required="required">
                <button type="submit"  >ir a fecha</button>
            </form>    
        </div>
        <div>
            <form action="vistareservas.php" method="post">
                <input type="hidden" name="date" 
                <?php 
                    if(isset($_POST["date"])){        
                        $despues=date('Y-m-d',strtotime("next week",strtotime($_POST["date"]))); }
                    else{$despues=date('Y-m-d', strtotime('next week'));}
                echo("value='".$despues."'")?>>
                <button type="submit" class="fechad" >Semana siguiente</button>
            </form>    
        </div>
    </div>
    <div class="menubutons">
        <div>
            <form action="./eliminarreserva.php" class="form_register" >
                <input type="submit" value="Eliminar Reserva" class="btn_registerE">
            </form>
        </div>
    </div>
    </section>
    <?php
        $reserva=new ReservasView();
        if(isset($_POST["date"])){
            $reserva->verReservasSemana2($_POST["date"]);
        }
        else{$reserva->verReservasSemana2(date("Y-m-d"));}
    ?>
    <br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>

    <?php include "./include/footer.php" ?>

</body>

</html>