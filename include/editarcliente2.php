<!doctype html>
<head>
<title>Editar cliente | EasySpa</title>
</head>
<body>
    <?php include_once "../classes/Dbh.class.php" ?> 
    <?php include_once "../classes/model/Clientes.class.php" ?> 
    <?php include_once "../classes/contr/ClientesContr.class.php" ?>
    <?php session_start(); ?>
    <?php
    $nombrescliente=$_POST['c_nombres'];
    $apellidoscliente=$_POST['c_apellidos'];
    $rutcliente=$_POST['c_rut'];
    $emailcliente=$_POST['c_email'];
    $telcliente=$_POST['c_telefono'];
    $tipocliente=$_POST['c_tipo'];
    $comentariocliente=$_POST['c_comentario']; 
    $idcliente=$_POST['idcliente'];      
    $cliente=new ClientesContr();
    $cliente->EditarCliente($nombrescliente,$apellidoscliente,$rutcliente,$emailcliente,$telcliente,$tipocliente,$comentariocliente,$idcliente);
    header("location: ../listaclientes.php?status=editado");
    
    exit();
    ?>
</body>
</html>