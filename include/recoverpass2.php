<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>EasySpa:Sistema de Administracion de Spa</title>   
</head>

<body>    
    <?php include_once "../classes/Dbh.class.php" ?> 
    <?php include_once "../classes/model/Personal.class.php" ?> 
    <?php include_once "../classes/contr/PersonalContr.class.php" ?>     
    <?php include_once "./PhpMailer/PHPMailer.php" ?>      
    <?php include_once "./PhpMailer/SMTP.php" ?>   
    <?php include_once "./PhpMailer/OAuth.php" ?>   

    <?php       
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;    
    if(isset($_POST["forgotsent"])){
        $selector = bin2hex(random_bytes(8));
        $token =random_bytes(32);
        $link="http://146.83.198.35:1062/recoverpass.php?selector=";
        //$link="http://localhost/g03-easyspa/recoverpass.php?selector=";
        $url =$link. $selector ."&validator=".bin2hex($token);
        $expire =date("U") +1800;
        $personalContr =new PersonalContr();
        $personalContr->eliminarToken($_POST["user"]);
        if($personalContr->ExisteUsuario($_POST["user"])!==false){
            $hash=password_hash($token,PASSWORD_DEFAULT);
            $personalContr->crearToken($_POST["user"],$selector,$hash,$expire);          
            $mail = new PHPMailer;
            $mail->isSMTP(); 
            $mail->SMTPDebug = 0; // 0 = off (for production use) - 1 = client messages - 2 = client and server messages
            $mail->Host = "smtp.gmail.com"; 
            $mail->Port = 587; // TLS only
            $mail->SMTPSecure = 'tls'; // ssl is depracated
            $mail->SMTPAuth = true;
            $mail->Username = "easyspamail@gmail.com";
            $mail->Password = "valparaiso2020";
            $mail->setFrom("easyspamail@gmail.com", "Soporte EasySpa");
            $mail->addAddress($_POST["user"],$_POST["user"]);
            $mail->Subject = 'Recuperacion acceso EasySpa';
            $mail->msgHTML("<p>Recibimos una solicitud de cambio de contraseña para EasySpa, si no realizo esta solicitud, la puede ignorar</p></br><p>Aqui esta el link para Reiniciar la contraseña:</p></br><a href=\"".$url."\">".$url."</a>");
            $mail->send();
            header("location: ../index.php?status=mailenviado");
            exit(); 
        }
        else {
            header("location: ../recoverpass.php?status=noexiste");
            exit();
        }
    } 
    else if (isset($_POST["changepass"])){
        $selector=$_POST["selector"];
        $validator=$_POST["validator"];
        $pass=$_POST["pass"];
        $passrepeat =$_POST["passrepeat"];
        if($pass!=$passrepeat){
            header("location: ../recoverpass.php?selector=$selector&validator=$validator&status=passnocoincide");
            exit();
        }
        $tiempoactual=date("U");
        $personalContr=new PersonalContr();
        $busqueda=$personalContr->buscarToken($selector,$tiempoactual);
        if($busqueda===false){
            header("location: ../recoverpass.php?status=tiempoexpirado");
            exit();
        }
        else{
            $validatorbin=hex2bin($validator);
            $checkvalidator=password_verify($validatorbin,$busqueda["resettoken"]);
            if($checkvalidator===false){
                header("location: ../recoverpass.php?status=validacionincorrecta");
                exit();
            }
            else if ($checkvalidator===true){
                $user=$personalContr->ExisteUsuario($busqueda["emailpersonal"]);
                if($user===false){
                    header("location: ../recoverpass.php?status=noexiste");
                    exit();
                }
                else{
                    $personalContr->CambiarClaveUsuario($busqueda["emailpersonal"],$pass);
                    header("location: ../index.php?status=clavecambiada");
                    exit();
                }
            }
        }       
    }
    else{
        header("location: ./index.php");
        exit();    
    }
        
          
             
    
    ?>   
    
    


</body>

</html>