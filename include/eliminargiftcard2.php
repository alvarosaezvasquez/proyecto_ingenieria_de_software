<!doctype html>
<head>
<title>Eliminar Giftcard | EasySpa</title>
</head>
<body>
    <?php include_once "../classes/Dbh.class.php" ?>
    <?php include_once "../classes/model/Giftcard.class.php" ?>
    <?php include_once "../classes/contr/GiftcardContr.class.php" ?>
    <?php session_start(); ?>
    <?php
    
    $idgiftcard=$_POST['idgiftcard'];

    $giftcard=new GiftcardContr();
    $giftcard->eliminarGiftcard($idgiftcard);
    header("location: ../giftcard.php?status=eliminado");

    exit();
    ?>
</body>
</html>