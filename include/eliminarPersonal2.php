<!doctype html>
<head>
<title>Eliminar Personal | EasySpa</title>
</head>
<body>
    <?php include_once "../classes/Dbh.class.php" ?>
    <?php include_once "../classes/model/Personal.class.php" ?>
    <?php include_once "../classes/contr/PersonalContr.class.php" ?>
    <?php session_start(); ?>
    <?php

    $idpersonal=$_POST['idpersonal'];

    $personal = new PersonalContr();
    $personal->borrarPersonal($idpersonal);
    header("location: ../personal.php?status=eliminado");

    exit();
    ?>
</body>
</html>