<?php 
    spl_autoload_register('autoCargaClases2');
    function autoCargaClases($className){ //carga las clases automaticamente , sin usar include_once,si el nombre de la clase es el mismo del archivo (considerando mayusculas)
        $path="classes/"; //solo sirve para archivos en la raiz del proyecto,  
        $extention =".class.php";
        $fullpath = $path . $className . $extention;
        $fullpath = str_replace('\\', '/', $fullpath);
        if(!file_exists($fullpath)){
            echo("no se encontro la clase en /classes, ¿seguro que el archivo y la clase tienen el mismo nombre, aun con mayusculas?");
            return false;
        }
        include_once $fullpath;
    }
    function autoCargaClases2($className){ //carga las clases automaticamente , sin usar include_once,si el nombre de la clase es el mismo del archivo (considerando mayusculas)
        $pre="../";
        $path="classes/"; //solo sirve para archivos en la raiz del proyecto,  
        $sub ="";
        $extention =".class.php";
        $fullpath = $path . $sub .$className . $extention;
        $fullpath = str_replace('\\', '/', $fullpath);
        if(!file_exists($fullpath)){
            $sub="model/";
            $fullpath = $path . $sub .$className . $extention;
            $fullpath = str_replace('\\', '/', $fullpath);
            if(!file_exists($fullpath)){
                $sub="contr/";
                $fullpath = $path . $sub .$className . $extention;
                $fullpath = str_replace('\\', '/', $fullpath);
                if(!file_exists($fullpath)){
                    $sub="view/";
                    $fullpath = $path . $sub .$className . $extention;
                    $fullpath = str_replace('\\', '/', $fullpath);
                    if(!file_exists($fullpath)){
                    echo("no se encontro la clase en /classes, ¿seguro que el archivo y la clase tienen el mismo nombre, aun con mayusculas?");
                    return false;
                    }
                }
            }
        }
        require_once $fullpath;
    }

?>

