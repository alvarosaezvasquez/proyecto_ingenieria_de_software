<!doctype html>
    <head>
        <title>Editar personal | EasySpa</title>
    </head>
    <body>
        <?php include_once "../classes/Dbh.class.php" ?>
        <?php include_once "../classes/model/Personal.class.php" ?>
        <?php include_once "../classes/contr/PersonalContr.class.php" ?>
        <?php session_start(); ?>
        <?php
            $nombrepersonal=$_POST['nombrepersonal'];
            $apellidopersonal=$_POST['apellidopersonal'];
            $rutpersonal=$_POST['rutpersonal'];
            $cargopersonal=$_POST['cargopersonal'];
            $emailpersonal=$_POST['emailpersonal'];
            $telefonopersonal=$_POST['telefonopersonal'];
            $idpersonal=$_POST['idpersonal'];

            $personal = new PersonalContr();
            $personal->updatePersonal($nombrepersonal, $apellidopersonal, $rutpersonal,$cargopersonal, $emailpersonal, $telefonopersonal, $idpersonal);
            header ("location: ../personal.php?status=personalModificado");

            exit();
        ?>
    </body>


</html>