<!doctype html>
<head>
<title>Agendar reserva | EasySpa</title>
</head>
<body>
    <?php include_once "../classes/Dbh.class.php" ?> 
    <?php include_once "../classes/model/Servicio.class.php" ?> 
    <?php include_once "../classes/contr/ServicioContr.class.php" ?>
    <?php include_once "../classes/model/Reservas.class.php" ?>
    <?php include_once "../classes/contr/ReservasContr.class.php" ?>
    <?php include_once "../classes/model/Giftcard.class.php" ?>
    <?php include_once "../classes/contr/GiftcardContr.class.php" ?>
    <?php session_start(); ?>
    <?php
    $idcliente=$_POST['idcliente'];
    $nombreservicios=$_POST['nombreservicios'];
    $fechareserva=$_POST['fechareserva'];
    $horareserva=$_POST['horareserva'];
    $idgiftcard=$_POST['idgiftcard'];
    $reservas= new ReservasContr();
    $reservaexiste=$reservas->consultarReservas();
    $giftcards2= new GiftcardContr();
    $giftcard2ex=$giftcards2->consultarGiftcards();
    date_default_timezone_set('America/Santiago');
    if($fechareserva < date('Y-m-d')){
        header("location: ../agendarservicio.php?status=fechainvalida");
        exit();
    }
    if($fechareserva == date('Y-m-d')){
        if($horareserva < idate('H', time())){
            header("location: ../agendarservicio.php?status=fechainvalida");
            exit();
        }
    }
    foreach ($reservaexiste as $r):
        if($r['fechareserva'] == $fechareserva && $r['horareserva'] == $horareserva){
            header("location: ../agendarservicio.php?status=reservaexiste");
            exit();
        }
    endforeach;
    if($idgiftcard==0){
        $reserva=new ReservasContr();
        $reserva->RegistrarReserva($idcliente,$nombreservicios,$fechareserva,$horareserva,$idgiftcard);
        header("location: ../vistareservas.php?status=agendado");
        exit();
    }
    foreach ($giftcard2ex as $r3):
        if($r3['idgiftcard'] == $idgiftcard && $r3['idcliente'] == $idcliente && strcmp($r3['servicio'],$nombreservicios )==0 ){
            $reserva=new ReservasContr();
            $reserva->RegistrarReserva($idcliente,$nombreservicios,$fechareserva,$horareserva,$idgiftcard);
            header("location: ../vistareservas.php?status=agendado");
            exit();
        }
    endforeach;
   
    header("location: ../agendarservicio.php?status=giftcardincorrecta");
    
    exit();
    
    ?>
</body>
</html>