<!doctype html>
<head>
<title>Registrar cliente | EasySpa</title>
</head>
<body>
    <?php include_once "../classes/Dbh.class.php" ?>
    <?php include_once "../classes/model/Clientes.class.php" ?>
    <?php include_once "../classes/contr/ClientesContr.class.php" ?>
    <?php session_start(); ?>
    <?php
        $c_nombres=$_POST["c_nombres"];
        $c_apellidos=$_POST["c_apellidos"];
        $c_rut=$_POST["c_rut"];
        $c_email=$_POST["c_email"];
        $c_telefono=$_POST["c_telefono"];
        $c_tipo=$_POST["c_tipo"];
        $c_comentario=$_POST["c_comentario"];
        if (!filter_var($c_email, FILTER_VALIDATE_EMAIL)) {
            header("location: ../registrarclientes.php?status=emailInvalido");
            exit();
        }
        $cliente=new ClientesContr();
        $cliente->InsertarCliente($c_nombres,$c_apellidos,$c_rut,$c_email,$c_telefono,$c_tipo,$c_comentario);

        header("location: ../listaclientes.php?status=clienteRegistrado");

        exit();
    ?>
</body>
</html>