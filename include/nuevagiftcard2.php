<!doctype html>
    <head>
        <title>Creacion de Giftcard | EasySpa</title>
    </head>
    <body>
        <?php include_once "../classes/Dbh.class.php" ?>
        <?php include "../classes/model/Giftcard.class.php" ?>
        <?php include_once "../classes/contr/GiftcardContr.class.php" ?>
        <?php session_start(); ?>
        <?php

            $idcliente = $_POST['idcliente'];
            $nombreservicios = $_POST['nombreservicios'];
            $fechadesde = $_POST['fechadesde'];
            $fechahasta = $_POST['fechahasta'];
            $estado = 'false';


            date_default_timezone_set('America/Santiago');

            if($fechadesde < date('Y-m-d')){
                header("location: ../nuevagiftcard.php?status=fechainvalida");
            exit();
            }

            date_default_timezone_set('America/Santiago');

            if($fechahasta < date('Y-m-d')){
                header("location: ../nuevagiftcard.php?status=fechainvalida");
                exit();
            }

            $giftcard = new GiftcardContr();            
            $giftcard->insertarGiftcard($fechahasta, $estado, $fechadesde, $idcliente, $nombreservicios);


            header("location: ../giftcard.php?status=giftcardcreada");

            exit();

        ?>
        </body>
</html>