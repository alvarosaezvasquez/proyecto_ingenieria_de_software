<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>EasySpa:Sistema de Administracion de Spa</title>   
</head>

<body>    
    <?php include_once "../classes/Dbh.class.php" ?> 
    <?php include_once "../classes/model/Personal.class.php" ?> 
    <?php include_once "../classes/contr/PersonalContr.class.php" ?>  
    <?php  
    session_start();       
    if (isset($_POST["changepass"])){            
        $oldpass=$_POST["oldpass"];
        $newpass=$_POST["newpass"];
        $newpassrepeat =$_POST["newpassrepeat"];                
        if($newpass!=$newpassrepeat){
            header("location: ../cambiarpass.php?status=passnocoincide");
            exit();
        }
        $personalContr=new PersonalContr(); 
        $usuarioex = $personalContr->ExisteUsuario($_SESSION["emailpersonal"]);
        if($usuarioex === false){
            header("location: ../index.php?status=usuarioincorrecto");
            exit();
        }
        $hashpass=$usuarioex["clavepersonal"];
        $validar = password_verify($oldpass,$hashpass);
        if($validar===false){
            header("location: ../cambiarpass.php?status=passincorrecta");
            exit();
        }
        else{
            $personalContr->CambiarClaveUsuario($_SESSION["emailpersonal"],$newpass);
            header("location: ../cambiarpass.php?status=clavecambiada");
            exit();
        }                 
    }
    else{
        header("location: ./index.php");
        exit();    
    }
    ?>   
</body>

</html>