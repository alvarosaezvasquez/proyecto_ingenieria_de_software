<!doctype html>
<head>
<title>Editar Servicio | EasySpa</title>
</head>
<body>
    <?php include_once "../classes/Dbh.class.php" ?> 
    <?php include_once "../classes/model/Servicio.class.php" ?> 
    <?php include_once "../classes/contr/ServicioContr.class.php" ?>
    <?php session_start(); ?>
    <?php
    $nombreservicios=$_POST['s_nombres'];
    $descripcionservicios=$_POST['s_descripcion'];
    $precioservicios=$_POST['s_precio'];
          
    $servicio=new ServicioContr();
    $servicio->EditarServicio($nombreservicios,$descripcionservicios,$precioservicios);
    header("location: ../listaServicios.php?status=editado");
    
    exit();
    ?>
</body>
</html>