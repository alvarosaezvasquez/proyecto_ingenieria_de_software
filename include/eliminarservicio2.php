<!doctype html>
<head>
<title>Eliminar Servicio | EasySpa</title>
</head>
<body>
    <?php include_once "../classes/Dbh.class.php" ?>
    <?php include_once "../classes/model/Servicio.class.php" ?>
    <?php include_once "../classes/contr/ServicioContr.class.php" ?>
    <?php session_start(); ?>
    <?php
    $nombrescliente=$_POST['s_nombres'];

    $servicio=new ServicioContr();
    $servicio->EliminarServicio($nombrescliente);
    header("location: ../listaServicios.php?status=eliminado");

    exit();
    ?>
</body>
</html>