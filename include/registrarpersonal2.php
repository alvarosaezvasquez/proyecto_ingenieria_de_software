<!doctype html>
    <head>
        <title>Registro de personal | EasySpa</title>
    </head>
    <body>
        <?php include_once "../classes/Dbh.class.php" ?>
        <?php include_once "../classes/model/Personal.class.php" ?>
        <?php include_once "../classes/contr/PersonalContr.class.php" ?>
        <?php session_start(); ?>
        <?php

            $nombrepersonal = $_POST['nombrepersonal'];
            $apellidopersonal = $_POST['apellidopersonal'];
            $rutpersonal = $_POST['rutpersonal'];
            $cargopersonal = $_POST['cargopersonal'];
            $emailpersonal = $_POST['emailpersonal'];
            $telefonopersonal = $_POST['telefonopersonal'];
            $clavepersonal = $_POST['clavepersonal'];
            if(!filter_var($emailpersonal, FILTER_VALIDATE_EMAIL)){
                header("location ../registrarpersonal.php?status=emailInvalido");
                exit();
            }

            $newpersonal = new PersonalContr();
            $newpersonal->InsertarPersonal($nombrepersonal, $apellidopersonal, $rutpersonal, $cargopersonal, $emailpersonal, $telefonopersonal, $clavepersonal);
            header("location: ../personal.php?status=personalRegistrado");

            exit();

        ?>
        </body>
</html>