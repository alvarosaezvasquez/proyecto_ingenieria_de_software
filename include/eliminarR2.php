<!doctype html>
<head>
<title>Eliminar Personal | EasySpa</title>
</head>
<body>
    <?php include_once "../classes/Dbh.class.php" ?>
    <?php include_once "../classes/model/Reservas.class.php" ?>
    <?php include_once "../classes/contr/ReservasContr.class.php" ?>
    <?php session_start(); ?>
    <?php

    $idreserva=$_POST['idreserva'];

    
    $reserva = new ReservasContr();
    $reserva->borrarReserva($idreserva);
    header("location: ../eliminarreserva.php?status=eliminado");
    exit();
    ?>
</body>
</html>