<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Clientes | EasySpa</title>   
    <link rel="stylesheet" href="css/listado.css">
</head>

<body>
    <?php include "./include/header.php"; ?>
    
    <h1>Lista de clientes</h1>

    <form action="./buscadorcliente.php" class="form_search" method="POST" >
        <input type="text" name="busqueda" id="busqueda" placeholder="Buscar cliente..." class ="bcliente" >
        <input type="submit" value="Buscar" class="btn_search">
    </form>

    <?php

        $busqueda = strtolower($_POST["busqueda"]);
        $cliente= new ClientesView();
        $resultado= $cliente->Buscadornombrecliente($busqueda);
        if($resultado === false){
            header("location: ./listaclientes.php?status=noencontrado");
            exit(); 
        }
        $cliente->VerBusquedaC($resultado);
    ?>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
        <?php include "./include/footer.php" ?>
</body>
</html>