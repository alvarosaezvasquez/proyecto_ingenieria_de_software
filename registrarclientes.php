<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Registrar cliente | EasySpa</title>
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="css/formulariopass.css">
    <link rel="stylesheet" href="css/formulario.css">

</head>
<body>
<?php include "./include/header.php" ?>
	
    <?php
    if(isset($_GET["status"])){
        if($_GET["status"]== "emailInvalido"){
            echo("<p>Este email no es válido</p>");
        }
    }

    ?>
    <section id="container">
    <h1>Registrar cliente</h1>
        <div class="form_register">
            <form action="./include/registrocliente.php" method="post">
            <input type="text" name="c_nombres" placeholder="Nombres" required="required" />
            <input type="text" name="c_apellidos" placeholder="Apellidos" required="required" />
            <input type="text" name="c_rut" placeholder="Rut" required="required"/>
            <input type="email" name="c_email" placeholder="Email" required="required" />
            <input type="number" name="c_telefono" placeholder="Telefono" required="required" />
            <input type="text" name="c_tipo" placeholder="Tipo" required="required" />
            <input type="text" name="c_comentario" placeholder="Comentario (opcional)" />
            <button type="submit" name="registrar" class="btn" >Registrar</button>
    </form>

        </div>
    </section>
    

    <br><br><br><br><br><br><br><br>
    <?php include "./include/footer.php" ?>

</body>

</html>