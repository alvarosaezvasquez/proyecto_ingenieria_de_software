<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Giftcards | EasySpa</title>
    <link rel="stylesheet" href="css/alpha.css">

</head>

<body>
    <?php include "./include/header.php" ?>
    <?php include "./classes/contr/GiftcardContr.class.php" ?>
    <?php include "./classes/contr/ClientesContr.class.php" ?>

    <section class= "contenedor">
    <h1>Lista de Giftcards</h1>
    <div>
        <form action="./nuevagiftcard.php" class="form_register" >
            <input type="submit" value="Nueva Giftcard" class="btn_registerP">
        </form>
        <br>
        <br>
        <br>
    </div>
    </section>

    <?php
    if(isset($_GET["status"])){
        if($_GET["status"]== "giftcardcreada"){
            echo("<p class=\"alert\">Giftcard creada</p>");
        }
        if($_GET["status"]== "eliminado"){
            echo("<p class=\"alert\">Giftcard eliminada correctamente</p>");
        }
    }
    ?>

    <?php
    $giftcard = new GiftcardView();
    $giftcard->verGiftcard();
    ?>

    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br>
    <?php include "./include/footer.php" ?>

</body>

</html>